# Status 

Working on Linux Debian however other systems are not tested + this 
Debian build is somewhat jank and missing support for a lot of the
API.

# Client Build

This will be where we keep the code for the web-based-client.
This client comms with our server code base found under `/server/` in this repository.

## Client Config Description

For unix based systems configuration will be found at `$HOME/.config/freechat-client/config.json`.
There a few top levle items that I will describe here.
