const $ = require('jquery')

// Path is from pages/index.html
const auth = require('../src/auth.js')
const message = require('../src/messages.js')
const settings = require('../src/settings.js')


$('#add-admin-json').click(async () => { await settings.add_server_to_config() } )

auth.init()
.then(
	value => console.log('Got config', value),
	reason => console.log('Failure in IPC call', reason) 
)
