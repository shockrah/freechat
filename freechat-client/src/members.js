const got = require('got')

/**
 * @param {String} auth.jwt
 * @param {u64} auth.id
 *
 * @param {String server.protocol
 * @param {String} server.hostname
 * @param {u16} server.port
 *
 * @param {u64} id
 *
 * @return {Sucess} {id: u64, name: String, joindate: i64, permissions: u64}
 * @returns {Failure} null
 */
exports.get_member = async function(auth, server, id) {
	try {
		const url = `${server.protocol}://${server.hostname}:${server.port}/members/single`
		const response = await got(url, {
			searchParams: { id: auth.id, jwt: auth.jwt, member_id: id },
			responseType: 'json',
		})

		return response.body['member']
	} catch (err) {
		return null
	}
}

