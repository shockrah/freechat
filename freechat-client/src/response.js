exports.Response = class Response {
    constructor(code, body, err) {
        this.status_code = code
        this.body = body
        this.err = err
    }
}
