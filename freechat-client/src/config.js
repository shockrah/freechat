const { ArgumentParser } = require('argparse')
const fs = require('fs').promises
const syncFs = require('fs')
const JSONBig = require('json-bigint')({storeAsString: true})

class Config {
	/**
	 * 
	 * TODO: change .data out for something not stupid
	 * @param {String} path path to config on startup
	 * @param {Object} data Contains actual config data
	 */
	constructor(path, data) {
		this.path = path
		this.data = data
	}

	async write_disk() {
		const str = JSONBig.stringify(this.data, null, 4)
		await fs.writeFile(this.path, str)
	}
}
exports.Config = Config

/**
 * Gets the config data off disk
 * @param {ArgumentParset} parser
 * @returns {Config}
 */
exports.from_cli_parser = function(parser) {

	const args = parser.parse_args()
	const defaultpath = `${process.env.HOME || process.env.USERPROFILE}/.config/freechat/config.json`

	// We'll just assume that the config is there as per the installation process
	const path = (args['config']) ? args['config'] : defaultpath
	let ret = new Config(path, {})

	// Allow errors from here to bubble up instead of handling them
	fs.readFile(path).then(file => {
		const json = JSONBig.parse(file)
		ret.data['servers'] = []

		// TODO: redo this so its let 'yolo'
		for(const serv of json['servers']) {
			ret.data['servers'].push(serv)
		}
	})
	.catch(err => console.error(err))

	return ret
}

