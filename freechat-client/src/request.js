const got = require('got')
const { Response } = require('./response.js')

/**
 * 
 * @param {String} method GET POST DELETE
 * @param {String} domain
 * @param {Number} port
 * @param {String} path
 * @param {Object} Query options to pass into a query string
 * @param {Buffer} Optional body that defaults to an empty string
 * 
 * @returns Response
 * @throws HTTPError
 */

exports.Request = async function (method, domain, port, path, params, body=null) {
	let url = `https://${domain}:${port}${path}`
	const options = {
		method: method,
		searchParams: params,
		responseType: 'json',
		body: body,
	}

	// NOTE: only in dev environments do we use http
	if(process.env['DEV_ENV']) {
		url = `http://${domain}:${port}${path}`
	}


	const resp = await got(url , options)
	return new Response(
		resp.statusCode,
		resp.body,
	)
}

/*
 * @param {String} proto 	| http/https
 * @param {String} hostname | 
 * @param {Number} port 	| 
 *
 * @description Helper macro 
 */
exports.BuildUrl = function(proto, hostname, port) {
	return `${proto}://${hostname}:${port}`
}
