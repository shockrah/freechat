#!/bin/sh

sassDir='sass/'
cssDir='pages/css/'

build() {
    for i in $sassDir/*;do
        echo Building: $i
        sass $i $cssDir/`basename -s .scss $i`.css
    done
}

watch() {
    pairs=''
    for i in $sassDir*;do
        pairs="$i:$cssDir`basename -s .scss $i`.css $pairs"
    done
    sass --watch $pairs
}

"$@"
