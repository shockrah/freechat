#!/bin/sh

# Simple test to make sure we can actually remove stuff from mysql
# without the whole thing complaining about everything 


. ./bin/activate

export DATABASE_URL=mysql://freechat_dev:password@localhost:3306/freechat
export DATABASE_NAME=freechat
export DATABASE_PASS=password
export DATABASE_USER=freechat_dev
export DATABASE_HOST=localhost
export DATABASE_PORT=3306

python invites-routine.py

