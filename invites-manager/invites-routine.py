# Chronjob which is meant to remove old/unwanted invites from our database


import mysql.connector as mysql
from os import getenv
import time, sys

def remove_old(config):
    '''
    Removes invites that are out of date
    '''
    now = int(time.time())
    query = ('DELETE FROM invites WHERE expires < ?')

    conn = mysql.connect(**config)
    cursor = conn.cursor(prepared=True)

    cursor.execute(query, (now,))

    cursor.close()
    conn.close()
    print(f'[ {__file__} ] : Removed old invites with no errors')


if __name__ == '__main__':
    config = {
        'database': getenv('DATABASE_NAME'),
        'password': getenv('DATABASE_PASS'),
        'user': getenv('DATABASE_USER'),
        'host': getenv('DATABASE_HOST'),
        'port': int(getenv('DATABASE_PORT')),
    }
    for k in config: 
        if config[k] is None:
            print(f'[ {__file__} ] : {k} not set', file=sys.stderr)
    else:
        remove_old(config)

