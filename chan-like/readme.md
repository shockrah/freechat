# Chan-Like Addon

A simple plugin which can be used to give Freechat a more _chan-like_ experience where messages are not saved forever.

This is basically another chronjob script that will execute every few minutes or so, removing old content unles otherwise specified.


## Options and what they do

-h --help : Shows a help message then exits

-m MAX_AGE_DAYS --max-age-days MAX_AGE_DAYS : Sets how old messages are allowed to be to avoid deletion(in days)

Example `-m 7` deletes messages older than 7 days.

-f FILE_ENV --file-env FILE_ENV : **Required** path to config file. See example below.

```
# Lines starting with '#' are ignored as comments
# No spaces between the keyname and '=' sign and value on the right
# '"' marks are also not required
# Keys the script doesn't care about are ignored
DATABASE_URL=mysql://user-account-to-use-the-db:supersecretpassword123@db.host.provider.io:3306/dbname-example

# Below are all the required keys for the script to work, the rest are ignored

DATABASE_NAME=dbname-example
DATABASE_PASS=supersecretpassword123
DATABASE_USER=user-account-to-use-the-db
DATABASE_HOST=db.host.provider.io
DATABASE_PORT=3306
```
