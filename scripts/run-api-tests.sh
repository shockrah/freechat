#!/bin/bash

set -e
export DATABASE_URL=mysql://freechat_dev:password@localhost:3306/freechat

# Next the json api
pushd ./json-api/
	echo Building API server
	# Building without rtc support
	cargo build --release
	# Using the newly built binary directly 
	echo Running API server
	cargo run --release -- -H ../hmac.secret -s&
	json_pid=$!
	echo Cargo PID: $json_pid
	time=3
	echo Waiting $time seconds for cargo to catch up
	sleep $time

	# Next come back and startup the rest test client
	echo Setting up client
	source ./client-tests/bin/activate
	# Put this in the background in case it errors out
	echo Running client
	python3 client-tests/main.py
popd


kill $(pgrep json)
