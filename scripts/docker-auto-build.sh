#!/bin/sh
# Author: shockrah

# Building

cargo_targets="json-api"

no_cargo_cache=false
ask_help=false

# Statically built binaries make this part really easy
build_cargo_targets() {
	for target in $cargo_targets;do
		echo ================================================
		echo =
		echo =
		echo =
		echo =               Building $target              
		echo =
		echo =
		echo =
		echo ================================================
		cd $target

			mkdir -p bin/
			cargo build --release
			cp target/release/$target bin/

			if [ $no_cargo_cache = "true" ];then
				cargo clean
			fi

		cd ../
	done
}

create_sample_env() { 
cat > .env <<EOF
# Mysql key data
DATABASE_NAME=
DATABASE_PASS=
DATABASE_USER=
DATABASE_HOST=

# Server meta things
SERVER_NAME="Freechat Dev Server"
SERVER_DESCRIPTION="Server for sick development things"
SERVER_URL=localhost
SERVER_PORT=8888

EOF

}


show_help() {
cat <<EOF
Script for BUILDING(not running/installing services) all relevant server binaries
Primarily to be used in docker images

Available options:
	--no-cargo-cache
		Cost: Slower compilation times after the first build as everything must be rebuilt
		Benefit: less disk usage as it cleans up unused files after a 'cargo build --release'
EOF

}

for arg in $@;do
	if [ "$arg" = "--no-cargo-cache" ];then
		no_cargo_cache=true
	fi

	if [ "$arg" = "--help" ];then ask_help=true; fi
	if [ "$arg" = "-h" ];then ask_help=true; fi

done


if [ $ask_help = "true" ];then
	# show help and quit
	show_help
else
	# normal building process
	build_cargo_targets
	create_sample_env
fi


