#!/bin/bash

# Points to the folder that contains all pertinent database tables
# Each table directory contains:
# 	dir/
	#	up.sql
	#	down.sql


# First some parameter checks
if [ ! $1 ];then
	echo Failure: Insufficient arguments provided
	echo ./build-db.sh /migrations/folder/path
	exit 1
fi

# Log paths
migration_logs=sql-migration.log

migration_loc=$1

# Running defaults here because there's no point in protecting dummy data
DATABASE_NAME=freechat
DATABASE_PASS=password
DATABASE_USER=freechat_dev
DATABASE_HOST=localhost

# Next we setup the database user
mysql -e "CREATE USER '$DATABASE_USER'@'$DATABASE_HOST' IDENTIFIED BY '$DATABASE_PASS';" \
	>  $migration_logs 2>&1
# Yet another reason to not use this in prod to setup you're database
mysql -e "CREATE DATABASE $DATABASE_NAME" \
	>> $migration_logs 2>&1
mysql -e "GRANT ALL PRIVILEGES ON $DATABASE_NAME.* TO '$DATABASE_USER'@'$DATABASE_HOST';" \
	>> $migration_logs 2>&1

# First we'll setup the databse that we want to use
for dir in $(ls $migration_loc);do
	# Port field is ignored in this script as its straight up not used in Docker tests
	# NOTE: there should **not** be a space between -p and the database password
	echo Migrating $migration_loc/$dir 
	cat $migration_loc/$dir/up.sql

	mysql -u $DATABASE_USER -p$DATABASE_PASS \
		-h $DATABASE_HOST -D $DATABASE_NAME \
		< $migration_loc/$dir/up.sql >> $migration_logs 2>&1
done


