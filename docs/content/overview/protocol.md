---
title: Introduction
anchor: intro
weight: 6
---

The Freechat protocol is based on HTTP and currently outlines a _json-based_ API
hereafter referred to as just, the "JSON API". 
Due to the standards already in place with HTTP and JSON it is safe to assume
that this protocol be more of an outline for what HTTP endpoints to have and what kind of data is to be expected in exchanges.

* Endpoints listed in the [JSON API Endpoints section](#endpoints).

* Data structures are also outline under the [structures](#structures) section.

