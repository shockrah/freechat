---
title: Overview
anchor: overview
weight: 5
---

_For the source code refer to the official [Gitlab repository here](https://gitlab.com/shockrah/freechat/)_.

## What is Freechat

A platform for free and open chatting.
The code is free, the protocol is simple and its all for free, like free beer _and_ free as in freedom, now and always.

## Features

* Text channels - to keep discussion organized

* Simple invite system - just a simple invite link will do.

* Powerful configuration options

For server owners: Don't like a feature? Turn it off.
For users: connections over TLS/TOR/SOCK5 or whatever other standard you want to use is allowed.


## Upcoming Features

* **Voice channels** - Currently the only thing being waited on is catching up the official client to the REST API standard(like 1 week) if you're reading this.

* **Video channels** - This one is admittedly far off but will be coming after voice channels are done

## What Tech is Freechat built on?

Freechat and its standard is built on HTTP for the most part. 
Application message payloads are most typically found in the HTTP body as JSON data however, some HTTP endpoints do return any kind of JSON data as there is no real need to.
For this reason developers should familiarize themselves with the standard to avoid errors.

There is also a Websocket portion to the standard which allows for real time communications such as voice chat and live chat updates.

## About this page

This page basically outlines what data is sent to and from each HTTP endpoint as well as what common patterns can be found in the standard.
There is also section 
