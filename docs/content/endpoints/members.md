---
title: Members
anchor: members-ep
weight: 14
---

### `POST /members/me/nickname`

* Required permissions:

	* CHANGE_NICK

* Required query string parameters:

	* id: u64
	* jwt: String
	* name: String
		* New nickname to change to(if allowed)

* Returns
	* None

### `GET /members/me`

* Required query string parameters:

	* id: u64
	* jwt: String

* Returns:

	* Member
		* Contains all public data about your account on the server


Example
```
	> GET /members/me?id=123&jwt=...

	< { 
	<	"name": "nickname-here", 
	<	"id": 123, 
	<	"permissions": <64-bit-mask>
	< }
```

### `GET /members/single`

* Required query string parameters:

	* id: u64
	* jwt: String

	* member_id: u64

* Returns
	* Full Public User
		* Behind the key 'member`

Example
```
	> GET /members/single

	< { 
	<	"name": "nickname-here", 
	<	"id": 0, // always 0
	<   "status": 1|2
	<	"permissions": <64-bit-mask>
	< }

```
		

### `GET /members/online`

* Required query string parameters:

	* id: u64
	* jwt: String
	* limit: Optional<u64>
		* Internal default: 100

* Returns
	* members: Array<Members>
		* Each member contains: [`name`, `id`, `permissions`]

	* count: u64


Example
```
	> GET /members/online?id=123&jwt=...

	< {  "members": [...] }
```



