---
title: Channels
anchor: channels-ep
weight: 13
---

### `GET /channels/list`

* Required query string parameters:

	* id: u64
	* jwt: String

* Required Permissions

	* None

* Returns:

	* channels: Array\<Channel\>

Example
```
	> GET /channels/list?id=123&jwt=...

	< {
	< 	"channels": [
	< 		{
	<			"id": 1
	<			"name": "general",,
	<			"kind": 2,
	<			"description": "this could be null"
	<		},
	<		...
	<	]
	< }
```

### `POST /channels/create`

* Required Permissions:

	* CREATE_CHANNEL

* Required query string parameters:

	* id: u64
	* jwt: String

	* name: String
	* kind: u32

		* Voice channel = 1
		* Text channel = 2

	* description: String
		* Optional field

* Returns

	* Channel
		* id: u64
		* name: String
		* description: String | null
		* kind: i32

```
	> POST /channels/create?id=123&jwt=...

	< { 
	<	"id": 3, 
	<	"name": "new-channel", 
	<	"description": null, 
	<	"kind": 2 
	< }
```

### `DELETE /channels/delete`

* Required permissions:

	* DELETE_CHANNEL

* Required query string parameters:

	* id: u64
	* jwt: String
	* channel_id: u64

* Returns:

	* None


