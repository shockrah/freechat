---
title: Authorization
anchor: auth-ep
weight: 11
---

To make things easy: user `id`, `secret`, and `jwt` values are to be put in the query string. Headers are largely ignored as they offer no guarantees in security. The biggest reason for this however is for simplicity of implementation for servers.

### `POST /login`

This route can be used to 
* Required query string parameters:

	* id: u64
	* secret: String

* Returns:

	* jwt: String


Example
```
	> POST /login?id=123&secret=super-secret-token

	< {"jwt": "asdf.asdf.asdf"}
```
