---
title: Messages
anchor: messages-ep
weight: 15
---

## Messages

### `POST /message/send`

* Required permissions:
	* SEND_MESSAGES

* Required query string parameters:
	* id: u64
	* jwt: String

	* channel_id: u64
* Required headers:
	Note that for any content that it is required that non-`text/plain` content be base64 encoded
	* content-type: String
		* text/plain
		* image/jpg | image/jpeg
		* image/png
		* application/webm
		* application/mp4
		* application/mp3

* Required body: 
	* Content itself should always go in the body
	* Empty bodies result in an HTTP 400 response

* Returns:
	* None

### `GET /message/get_range`

* Required query string parameters:
	* id: u64
	* jwt: string

	* channel_id: u64
	* start_time: i64
		* Unix timestamp (milli-seconds)
	* end_time: i64
		* Unix timestamp (milli-seconds)
	
	* limit: Optional<u64>
		* Maximum = 1000
	 
* Returns
	* messages: Array<Message>


Example
```
	> GET /message/get_range?id=123&jwt=...

	< { "mesages": [...] }
```


### `GET /message/from_id`

* Required query string parameters:
	* id: u64
	* jwt: string

	* channel_id: u64
	* start: u64
	* limit: Optional<u64>
		* Maximum = 1000

* Returns 
	* messages: Array<Message>

Example
```
	> GET /message/from_id?id=123&jwt=...

	< { "mesages": [...] }
```

