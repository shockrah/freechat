---
title: Users
anchor: users
weight: 50
---

### Personal User

If you need to get your own user data, with `/users/me` then the data received on success has the following fields:

Name    | Type
:------:|:-----:
 id     | `u64`
 secret | `String`
status  | `i32`
permissions| `u64`
badge\_ids | `Array<u32>`

### JWT

This data is retrieved after a successful `/login` hit.

  Name |  Type
:-----:|:------:
  jwt  | `String`

### Full Public User

  Name | Type
:-----:|:-----:
  id   | `u64`
name   | `String`
status  | `i32`
permissions| `u64`
badge\_ids | `Array<u32>`

For users _without_ the `USER_FETCH` permission the only two fields with actual data is `id` and `name`. The other fields will be set to 0.

