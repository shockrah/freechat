---
title: Messages
anchor: messages-struct
weight: 40
---

  Name | Type
:-----:|:-----:
   id      | `u64`
  time     | `i64`
 content   | `String`
  content_type     | `String`
author_id  | `u64`
channel_id | `u64`
 

Acceptable values 

## Invites

When requesting a code from `/invite/create` successful data contains:

  Name | Type
:-----:|:-----:
  id   | `i64`
 uses  | `null|i64`
expires| `bool`

Most clients should format this code for usage as `https://domain.net:port/join?code=<id>`. The other data is merely for tracking the server's end.


