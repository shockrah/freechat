---
title: Structures
anchor: structures
weight: 30
---

This section details what kind of data structures are returned by the json-api.

**IMPORTANT**: All structures are returned in [JSON form](https://www.json.org/json-en.html).

## General Data Notation

* u64

	* Unsigned 64 bit integer

* i64

	* Signed 64 bit integer

* String

## Channels

Channels are made up of the following components

 Name | Type 
:------:|:--------:
 id   | `u64`
 name | `String`
 kind | `i32`


Channels have two valid types or (more semantically) `kind` values: 

* Voice Channel = 1

* Text Channel = 2

## Users

### Personal User

If you need to get your own user data, with `/users/me` then the data received on success has the following fields:

Name    | Type
:------:|:-----:
 id     | `u64`
 secret | `String`
joindate| `i64`
status  | `i32`
permissions| `u64`

### JWT

This data is retrieved after a sucessful `/login` hit.

  Name |  Type
:-----:|:------:
  jwt  | `String`

### Full Public User

  Name | Type
:-----:|:-----:
  id   | `u64`
name   | `String`
joindate| `i64`
status  | `i32`
permissions| `u64`

For users _without_ the `USER_FETCH` permission the only two fields with actual data is `id` and `name`. The other fields will be set to 0.

## Messages

  Name | Type
:-----:|:-----:
   id      | `u64`
  time     | `i64`
 content   | `String`
  type     | `String`
author_id  | `u64`
channel_id | `u64`
 

Acceptable values 

## Invites

When requesting a code from `/invite/create` successful data contains:

  Name | Type
:-----:|:-----:
  id   | `i64`
 uses  | `null|i64`
expires| `bool`

Most clients should format this code for usage as `https://domain.net:port/join?code=<id>`. The other data is merely for tracking the server's end.



