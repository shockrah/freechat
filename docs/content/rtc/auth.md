---
title: RTC Authorization
anchor: rtc-auth
weight: 41
---

## For server devs

If you would like to follow the same model that the current build uses for 
RTC notifications then simply have the REST API server use the `wss-hmac.secret`
to authenticate a "special" connection to the web socket server which is allowed
to broadcast messages to the user connections.
It should be noted that currently user connections that send messages after the
connection has been established are ignored. _The server is really the only one that 
can actually broadcast messages to users_.

The architecture for the web socket server is as follows: 

![Basic Architecture](/wss-arch.png)

## For client devs

To authenticate properly you must use the following url structure:

> `<wsurl>/text?jwt=<jwt>`

> Example ws://localhost:1234/text?jwt=asdf123


The JWT in the query string is the same JWT used for requests against the REST API.
The `/text` path listens for changes on all available text channels so even if a 
