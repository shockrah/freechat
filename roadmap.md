# Development Roadmap

Things to keep in mind:

* Version numbering doesn't really mean much but are just used to keep track of some milestones in the roadmap

* Larger milestones are named, not numbered, such as the "VC Update" which marks a significant milestone has been reached

* While effort has been put in to come up with a "_concrete_" roadmap there is still the chance that things may change



## App changes

These are changes/milestones that pertain to the application users download, the _frontend_ if you will.

* File upload, message caching and file download/preview. Currently recent messages are fetched but sometimes even older messages

* Aggro-Caching: caching old messages(up to some limit) on disk to fill out some space.

* Loading UI: anything that takes more than a second should have some UI that tells us its saving/dl'ing/loading etc.

* Have a system for showing community made Apps that are well polished.

The API is made to be super simple so its likely that some people will want to make their own programs to communicate in Freechat instances.
I'd like to be able to support people that make good software and give them a platform to show off the work they've done.

## Backend Server Changes

* Badge System: Similar to real life badges Freechat's badge system will basically server as a way to "mark" users with special traits. For example: people in certain subgroups in a community may be inclined to have a common badge for permission reasons or other such things.

	* It should be noted that fetching server badges should be an unprivileged act, thus any client should be able to get the full list off available badges and cache the result as they see fit.

	* By default all users will have global permissions under their permissions field which is to be used for more drastic/sparing things.(It's likely that we can reduce the size of the bitmask size to 32bits as well since there's like not many actual rules we want to be global.

	* Badges will likely be used for permissions so they should be stored somewhere where we can avoid db hits whenever possible.



* ~~More server meta endpoint support: Some basic server metadata is stored in environment vars but we can probably store these somewhere a bit smarter idk.~~

	* ~~This is now actively being worked with /neighbor/list and /neighbor/add now being added to the supported routes. Further testing is required, as well as relevant documentation. At least this should be done within the next few days as I find time for it among other projects.~~

* RTC Permissions: Unprivileged users shouldn't receive messaegs if they don't have the required permissions


* ~~Optional RTC: Optionally compile RTC communications(Easy)~~

* Optional decentralization: Optionally compile decentralization comms to users. All of this can be done at the API level(Probably easy).

* More RTC testing: We need to start testing RTC communications to make sure events are pushed correctly



## Major System Level Updates

## Nice to have's

These aren't really required or can be worked around with minor effort


* More efficient auth caching: 

Right now the authentication system uses some MySql to authenticate API token permissions.
Of course this is slow as hell becauses we could be using a much leaner caching system. A simple cache could be written from scratch to avoid reduce back-pressure in distributed setups but that is a more complex decision to make later down the line.
