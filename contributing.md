# Contributing to the project

Check the [roadmap](/roadmap.md) to see what specifically is being worked on and what could use more help.

## Languages/Tech Stack - Rust/Python/Bash/Javascript/HTML/CSS

Here's a quick run down of the tech-stack:

* json-api: Written with Rust

* json-api/client-tests/: A python based mock client used to verify the behavior of the REST API

* rtc-server/: A web socket server with NodeJS

This optional feature provides real time chatting. Future plans also include voice chat as an additional feature.

* freechat-client/: Electron based client _(that is always behind the backend'd development)_

The whole point of this client is to give regular users something that works on every platform.

* tui/: Rust based text client _(Currently very far behind backend's development)_

The purpose with this tui is to be a keyboard driven program to be used in a terminal.

* invites-manager/: Python script that acts as a simple cron job, removing old/stale invites

* chan-like/: Similar to the invites-manager but written in Rust, made to remove old messages similar to how a modern image board removes old threads

In part the purpose with this is to keep the amount of data stored in the database to a minimum but also because not everyone
is going to want messages to persist forever. This feature is optional.

## Writing chat clients

For a reference to the Freechat API check the [wiki](https://freechat.shockrah.xyz/).

The REST API aims to be as stable as possible in behavior as well as straight forward as possible.
This should hopefully make it easier to develop simple client programs.

The electron based client and the tui client are meant to be the two officially supported client programs right now however they are not in usable states at the moment.

## Submitting Patches

The preferred method is through merge requests, however, for some special cases you may send me patch files at the email listed [here.](https://shockrah.xyz/links.html)
Individual changes in a given patch should be noted with the following things:

* A short one line sumary prefixed with a `*`, `+`, `-`, `!`

    * `*` Marks a 1 to 1 change
    * `+` Marks some addition
    * `-` Marks some removal
    * `!` Marks an important note about one of the above, similar to a warning regarding a patch note

* (If applicable) A more detailed summary/explanation regarding the patch

This is where context can be given regarding why this patch is being applied and what details are important to quickly understanding the patch details.
**NOTE**: if the patch is really tiny (a simple typo fix for instance) this part may not be required.
