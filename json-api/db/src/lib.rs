use serde::{Serialize, Deserialize};

pub mod member;
pub mod common;
pub mod invites;
pub mod channels;
pub mod messages;
pub mod neighbors;
pub mod badges;
pub mod jwt;

use std::vec::Vec;

pub type BigInt = i64;
pub type UBigInt = u64;

pub type Integer = i32;
pub type UInteger = u32;

pub type VarChar = String;

#[derive(Debug)]
pub enum Response<T> {
    // A set of rows collected
    Set(Vec<T>),
    // Single row collected
    Row(T),
    // Nothing was found -> for select/update/delete's
    Empty,
    // Generic success for things like update/delete's
    Success,
    // Mask 500's with probable user generated errors like duplicate keys being added
    // or data that doesn't fit in column or something
    RestrictedInput(String),
    // Not sure what this will be used for but maybe its useful at some point
    Other(String)
}

#[allow(dead_code)]
#[derive(Debug, Serialize)]
pub struct Message {
    pub id: UBigInt,
    pub time: BigInt,
    pub content: VarChar,
    pub content_type: VarChar,
    pub author_id: UBigInt,
    pub channel_id: UBigInt
}

// Same EXCEPT for the fact that this has the name tacked on to it
#[derive(Debug, Serialize)]
pub struct UserMessage {
    pub id: UBigInt,
    pub time: BigInt,
    pub content: VarChar,
    pub content_type: VarChar,
    pub author_id: UBigInt,
    pub channel_id: UBigInt,
    pub name: VarChar
}



#[derive(Clone, Serialize)]
pub struct Channel {
    pub id: UBigInt,
    pub name: VarChar,
    pub description: Option<VarChar>,
    pub kind: Integer,
    pub badge_ids: Vec<u32>
}

#[derive(Serialize, Debug)]
pub struct Invite {
    pub id: BigInt,
    pub uses: Option<BigInt>,
    pub expires: bool
}

#[derive(Debug, Serialize)]
pub struct Member {
    pub id: UBigInt,
    pub secret: VarChar,
    pub name: VarChar,
    pub status: Integer,
    pub permissions: UBigInt,
    pub badge_ids: Vec<u32>
}

#[derive(Serialize)]
pub struct PublicMember {
    pub id: UBigInt,
    pub name: VarChar,
    pub status: Integer,
    pub permissions: UBigInt,
    pub badge_ids: Vec<u32> // badge id's
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Neighbor {
    pub name: String,
    pub description: Option<String>,
    pub tags: Vec<String>,
    pub url: String,
    pub wsurl: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Badge {
    pub name: String,
    pub id: u64,
    pub color: u32,
    pub perms: u64,
}
