use crate::Neighbor;
use mysql_async::Result as SqlResult;
use mysql_async::{params,  Pool, prelude::Queryable};
use serde_json;

pub async fn get_all(p: &Pool) -> SqlResult<Vec<Neighbor>> {
    let mut conn = p.get_conn().await?;
    let q = "SELECT name, description, tags, url, wsurl FROM neighbors";
    type Types = (String, Option<String>, String, String, Option<String>);
    let set: Vec<Neighbor> = conn.exec_map(q, (), |(name, description, tags, url, wsurl): Types| {
        let json: Vec<String> = serde_json::from_str(tags.as_str()).unwrap_or(vec![]);
        Neighbor {
            name, description, tags: json, url, wsurl
        }
    }).await?;
    Ok(set)
}

pub async fn add_neighbor(p: &Pool, new: Neighbor) -> SqlResult<()> {
    // Note we assume that the tags field has been verified as proper valid json prior to 
    // writing it to the db
    let mut conn = p.get_conn().await?;
    let q = "INSERT INTO neighbors(url, wsurl, name, description, tags) 
        VALUES(:url, :wsurl, :name, :desc, :tags)";
    let tags = serde_json::to_string(&new.tags).unwrap_or("[]".to_string());
    let sqlparams = params!{
        "url"=> new.url,
        "wsurl"=>new.wsurl,
        "name"=>new.name,
        "desc"=>new.description,
        "tags"=>tags
    };
    conn.exec_drop(q, sqlparams).await?;
    Ok(())
}

pub async fn get(p: &Pool, url: &str) -> SqlResult<Option<Neighbor>> {
    let mut conn = p.get_conn().await?;
    let q = "SELECT wsurl, name, description, tags FROM neighbors 
        WHERE url = :url";

    type Fields = (Option<String>, String, Option<String>, String);
    let row: Option<Fields> = conn.exec_first(q, params!{"url" => url}).await?;
    if let Some(fields) = row {
        let tags: Vec<String> = serde_json::from_str(fields.3.as_str()).unwrap_or(vec![]);
        let n = Neighbor {
            name: fields.1,
            wsurl: fields.0,
            description: fields.2,
            tags,
            url: url.into(),
        };
        return Ok(Some(n));
    }
    Ok(None)
}

pub async fn update(p: &Pool, url: &str, new: Neighbor) -> SqlResult<()> {
    let mut conn = p.get_conn().await?;
    let q = "UPDATE neighbors 
        SET name = :nm, description = :desc, tags = :tags, wsurl = :ws, url = :newurl
        WHERE url = :url";

    let sqlparams = params!{
        "url" => url,
        "nm" => new.name,
        "desc" => new.description,
        "tags" => serde_json::to_string(&new.tags).unwrap(),
        "ws" => new.wsurl,
        "newurl" => new.url
    };

    conn.exec_drop(q, sqlparams).await?;
    Ok(())
}

pub async fn remove(p: &Pool, url: &str) -> SqlResult<()> {
    let mut conn = p.get_conn().await?;
    let q = "DELETE FROM neighbors WHERE url = :url";
    conn.exec_drop(q, params!{"url" => url}).await?;
    Ok(())
}
