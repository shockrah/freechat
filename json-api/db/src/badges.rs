use crate::Badge;
use mysql_async::prelude::Queryable;
use mysql_async::{params, Pool, Result as SqlResult};
use rand::RngCore;

pub async fn add(pool: &Pool, name: &str, color: u32, perms: u64) -> SqlResult<Badge>{
    // First gather the parameters required for the query
    let id = rand::rngs::OsRng.next_u64();

    let mut conn = pool.get_conn().await?;
    let q = "INSERT INTO badges (id, name, color, permissions)
        VALUES(:id, :name, :color, :perms)";
    let sqlparams = params!{
        "id" => id, 
        "name" => name, 
        "color" => color, 
        "perms" => perms
    };
    conn.exec_drop(q, sqlparams).await?;
    Ok(Badge{
        id,
        name: String::from(name),
        color,
        perms
    })
}

pub async fn delete(pool: &Pool, id: u64) -> SqlResult<u64> {
    let mut conn = pool.get_conn().await?;
    let q = "DELETE FROM badges WHERE id = :id";
    conn.exec_drop(q, params!{"id" => id}).await?;
    Ok(id)
}

async fn get(pool: &Pool, id: u64) -> SqlResult<Option<Badge>> {
    let mut conn = pool.get_conn().await?;
    let q = "SELECT name, color, permissions FROM badges WHERE id = :id";
    let row: Option<(String, u32, u64)> = conn.exec_first(q, params!{ "id" => id}).await?;
    match row {
        Some((name, color, perms)) => {
            let b = Badge {
                id,
                name,
                color,
                perms
            };
            Ok(Some(b))
        },
        None => Ok(None)
    }
}

async fn badge_update(pool: &Pool, id: u64, query: &'static str, values: mysql_async::Params) -> SqlResult<bool> {
    if let Some(_) = get(pool, id).await? {
        let mut conn = pool.get_conn().await?;
        conn.exec_drop(query, values).await?;
        Ok(true)
    } else {
        Ok(false)
    }
}

pub async fn update_perms(pool: &Pool, id: u64, perms: u64) -> SqlResult<bool> {
    // Ok(None) if that badge was not registered anywhere
    // Ok(Some(Badge)) if that badge was registered
    let q = "UPDATE badges SET permissions = :perms WHERE id = :id";
    let p = params!{"id" => id, "perms" => perms};
    return badge_update(pool, id, q, p).await;
}

pub async fn update_name(pool: &Pool, id: u64, name: &str) -> SqlResult<bool> {
    let q = "UPDATE badges SET name = :name WHERE id = :id";
    let p = params!{"id" => id, "name" => name};
    return badge_update(pool, id, q, p).await;
}

pub async fn update_color(pool: &Pool, id: u64, color: u32) -> SqlResult<bool> {
    let q = "UPDATE badges SET color = :color WHERE id = :id";
    let p = params!{"id" => id, "color" => color};
    return badge_update(pool, id, q, p).await;
}

pub async fn list(pool: &Pool) -> SqlResult<Vec<Badge>>  {
    let q = "SELECT id, name, color, permissions FROM badges";
    let mut conn = pool.get_conn().await?;
    let set: Vec<Badge> = conn.exec_map(q, (), |(id, name, color, perms)| {
        Badge { id, name, color, perms }
    }).await?;
    Ok(set)
}
