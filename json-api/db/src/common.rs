use crate::Response;
use mysql_async::Error;

#[macro_export]
macro_rules! no_conn {
    ($spec:literal) => {
        format!("[ CON Error ] : {}", $spec)
    }
}

#[macro_export]
macro_rules! sql_err {
    ($spec:literal) => {
        format!("[ SQL Error ] : {}", $spec)
    };

    // Using this mostly to pull in sql err types from lib to outside world for logging
    ($exp:expr) => {
        format!("[ SQL Error ] : {}", $exp)
    }
}

#[macro_export]
macro_rules! sql_err_log {
    ($spec:expr) => {
        println!($spec);
    }
}


pub fn try_error_passthrough<T>(err: Error) -> Result<Response<T>, Error> {
    // Some user input _will_ cause sql to complain about things like foreign key
    // constraints
    // In order to translate 500's into more precise 400's we use this function
    // (sparingly)
    match &err {
        Error::Server(se) => {
            if se.code == 1452 {
                return Ok(Response::RestrictedInput(format!("Invalid key value given")))
            }
            return Err(err);
        },
        _ => Err(err)
    }
}
