-- TODO: somehow make the name colum unique
CREATE TABLE IF NOT EXISTS `channels` (
    `id` BIGINT UNSIGNED NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(2048),
    `kind` INTEGER NOT NULL,
    `badge_ids` VARCHAR(2048) DEFAULT '[]',
    PRIMARY KEY(`id`), UNIQUE KEY(`name`)
);
