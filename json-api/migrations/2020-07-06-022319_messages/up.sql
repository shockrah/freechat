-- Time stamp is _not_ in ms
CREATE TABLE IF NOT EXISTS `messages`(
	`id` BIGINT UNSIGNED NOT NULL,
	`time` BIGINT NOT NULL,
	-- Assuming content-type is `not` text/plain this will be a reference to some auto-named file
	`content` VARCHAR(4096) NOT NULL,
	`content_type` VARCHAR(10) NOT NULL,
	`author_id` BIGINT UNSIGNED NOT NULL,
	`channel_id` BIGINT UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`author_id`) REFERENCES members(`id`),
	FOREIGN KEY (`channel_id`) REFERENCES channels(`id`) ON DELETE CASCADE
);
