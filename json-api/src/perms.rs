
// GENERAL PERMISSIONS
pub const JOIN_VOICE:u64 = 1;
pub const SEND_MESSAGES:u64 = 2;
pub const CHANGE_NICK:u64 = 16;
pub const ALLOW_PFP:u64 = 32; 
pub const CREATE_TMP_INVITES:u64 = 4; 
pub const CREATE_PERM_INVITES:u64 = 8; // to make perma invites you need both flags


pub const _ADMIN: u64 = 1 << 62; // can make other admins but can't really touch the owner

// ADMIN PERMS
pub const CREATE_CHANNEL:u64 = 64;
pub const DELETE_CHANNEL:u64 = 128;
pub const ADD_NEIGHBOR:u64 = 256;
pub const MOD_BADGE:u64 = 512;

// BELOW ARE COLLECTIVE PERMISSION SETS
pub const OWNER: u64 = std::u64::MAX;
pub const GENERAL_NEW: u64 = JOIN_VOICE | SEND_MESSAGES | ALLOW_PFP | CHANGE_NICK;
pub const ADMIN_PERMS: u64 = !(std::u64::MAX & OWNER); // filter the only perm admins don't get

pub fn get_perm_mask(path: &str) -> Option<u64> {
    use crate::routes::{
        self,
        INVITE_CREATE,
        CHANNELS_LIST, CHANNELS_CREATE, CHANNELS_DELETE,
        MESSAGE_SEND,
        NEW_BADGE, DELETE_BADGE, UPDATE_COLOR_BADGE, UPDATE_PERMS_BADGE, UPDATE_NAME_BADGE
    };
    match path {
        INVITE_CREATE => Some(CREATE_TMP_INVITES),

        CHANNELS_LIST => None,
        CHANNELS_CREATE => Some(CREATE_CHANNEL),

        CHANNELS_DELETE => Some(DELETE_CHANNEL),

        MESSAGE_SEND => Some(SEND_MESSAGES),

        NEW_BADGE|DELETE_BADGE|UPDATE_COLOR_BADGE|UPDATE_NAME_BADGE|UPDATE_PERMS_BADGE =>
            Some(MOD_BADGE),

        routes::ADD_NEIGHBOR => Some(ADD_NEIGHBOR),
        _ => Some(0)
    }
}

#[inline(always)]
pub fn has_perm(mask: u64, target: u64) -> bool {
    // Check if the given mask contains the target permissions mask
    return (mask & target) == target;
}
