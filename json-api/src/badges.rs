use crate::{db, qs_param, http::set_json_body};

use std::collections::HashMap;
use mysql_async::Pool;
use hyper::{Response, StatusCode, Body};
use serde_json::json;

pub async fn new(p: &Pool, response: &mut Response<Body>, params: HashMap<String, String>) {
    // Only name is really required the other two can default to white/
    let name = qs_param!(params, "badge_name", String);
    let perms = match qs_param!(params, "badge_perms", u64) {
        Some(perms) => perms,
        None => 0
    };
    let color = match qs_param!(params, "badge_color", u32) {
        Some(color) => color,
        None => 0xffffffff,
    };
    if let Some(name) = name {
        match db::badges::add(p, &name, color, perms).await {
            Ok(badge) => { 
                set_json_body(response, json!({"badge": json!(badge)}));
                #[cfg(feature = "rtc")]
                {
                    use crate::rtc;
                    if let Err(e) = rtc::notify("new-badge", badge).await {
                        eprintln!("[API-RTC] Unable to connect to RTC server {}", e);
                    }
                }
            },
            Err(e) => { 
                eprintln!("[HTTP][ERROR] /badge/new {}", e);
                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
            }
        };
    } else {
        *response.status_mut() = StatusCode::BAD_REQUEST;
    }
}

pub async fn update_perms(p: &Pool, response: &mut Response<Body>, params: HashMap<String, String>) {
    let id = qs_param!(params, "badge_id",  u64);
    let perms = qs_param!(params, "badge_perms", u64);
    if let (Some(id), Some(perms)) = (id, perms) {
        match db::badges::update_perms(p, id, perms).await {
            // TODO: add rtc update here
            Ok(true) => {
                let payload = json!({ "id": id, "perms": perms});
                set_json_body(response, json!({"badge-update": payload}));
                #[cfg(feature = "rtc")]
                {
                    use crate::rtc;
                    if let Err(e) = rtc::notify("badge-update-perms", payload).await {
                        eprintln!("[API-RTC] Unable to connect to RTC server {}", e);
                    }
                }
            },
            Ok(false) => {
                *response.status_mut() = StatusCode::NOT_FOUND;
            },
            Err(e) => {
                eprintln!("[HTTP][ERROR] /badge/update/perms {}", e);
                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
            }
        }
    } else {
        *response.status_mut() = StatusCode::BAD_REQUEST;
    }
}

pub async fn update_color(p: &Pool, response: &mut Response<Body>, params: HashMap<String, String>) {
    let id = qs_param!(params, "badge_id", u64);
    let color = qs_param!(params, "badge_color", u32);

    if let (Some(id), Some(color)) = (id, color) {
        match db::badges::update_color(p, id, color).await {
            Ok(true) => {
                // NOTE: this response iss more meant for rtc as the non-rtc mode
                // isn't supposed respond with anything in particular
                // TODO: rtc update here
                let payload = json!({"id": id, "color": color});
                set_json_body(response, json!({"badge-update": payload}));
                #[cfg(feature = "rtc")]
                {
                    use crate::rtc;
                    if let Err(e) =  rtc::notify("update-badge", payload).await {
                        eprintln!("[API-RTC] Unable to connect to RTC server {}", e);
                    }
                }
            },
            Ok(false) => {
                *response.status_mut() = StatusCode::NOT_FOUND;
            },
            Err(e) => {
                eprintln!("[HTTP][ERROR] /badge/update/color {}", e);
                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
            }
        }
    } else {
        *response.status_mut() = StatusCode::BAD_REQUEST;
    }
}

pub async fn update_name(p: &Pool, response: &mut Response<Body>, params: HashMap<String, String>) {
    let id = qs_param!(params, "badge_id", u64);
    let name = qs_param!(params, "badge_name", String);

    if let (Some(id), Some(name)) = (id, name) {
        match db::badges::update_name(p, id, &name).await {
            Ok(true) => {
                let  payload = json!({"id": id, "name": name});
                set_json_body(response, json!({"badge-update": payload}));
                #[cfg(feature = "rtc")]
                {
                    use crate::rtc;
                    if let Err(e) =  rtc::notify("update-badge", payload).await {
                        eprintln!("[API-RTC] Unable to connect to RTC server {}", e);
                    }
                }
            },
            Ok(false) => {
                *response.status_mut() = StatusCode::NOT_FOUND;
            },
            Err(e) => {
                eprintln!("[HTTP][ERROR] /badges/update/name {}", e);
                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
            }
        }
    } else {
        *response.status_mut() = StatusCode::BAD_REQUEST;
    }
}


pub async fn delete(p: &Pool, response: &mut Response<Body>, params: HashMap<String, String>) {
    if let Some(id) = qs_param!(params, "badge_id", u64) {
        match db::badges::delete(p, id).await {
            Ok(id) => {
                #[cfg(feature = "rtc")]
                {
                    use crate::rtc;
                    if let  Err(e) = rtc::notify("delete-badge", json!({"id": id})).await {
                        eprintln!("[API-RTC] Unable to connect to RTC server {}", e);
                    }
                }
            }, 
            Err(e) => {
                eprintln!("[HTTP][ERROR] /badge/delete {}", e);
                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
            }
        }
    } else {
        *response.status_mut() = StatusCode::BAD_REQUEST;
    }
}

pub async fn list(p: &Pool, response: &mut Response<Body>) {
    match db::badges::list(p).await {
        Ok(badges) => {
            set_json_body(response, json!({"badges": json!(badges)}));
        },
        Err(e) => {
            eprintln!("[HTTP][ERRO] /badge/list {}", e);
            *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
        }
    }
}
