use hyper::{StatusCode, Response, Body};

use mysql_async::Pool;

use serde_json::json;

use std::collections::HashMap;

use db::{ self, Channel};

use crate::http::set_json_body;
use crate::qs_param;


pub async fn list_channels(pool: &Pool, response: &mut Response<Body>, params: HashMap<String, String>) {
    /*
     * @kind : i32 1|2 => Specifies Voice or Text channels respectively
    */

    if let Some(chan_kind) = qs_param!(params, "kind", i32) {
        match db::Channel::filter(pool, chan_kind).await {
            Ok(resp) => match resp {
                db::Response::Set(channels) => set_json_body(response, json!({"channels": json!(channels)}) ),
                _ => *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR
            },
            Err(e) => {
                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                eprintln!("{}", e);
            }
        }
    } else {
        *response.status_mut() = StatusCode::BAD_REQUEST;
    }
}

pub async fn create_channel(pool: &Pool, response: &mut Response<Body>, params: HashMap<String, String>) {
    /*
     * Create a channel base on a few parameters that may or may not be there
     * @responds with the data of the newly created channel
    */
    // Theres an extra un-needed unwrap to be cut out from this proc
    // specifically with the desc parameter
    use db::Response::*;

    let name = params.get("name");
    let kind = qs_param!(params, "kind", i32);
    let description = match params.get("description") {
        Some(d) => d,
        None => "No description"
    };

    *response.status_mut() = if name.is_some() && kind.is_some() {
        let (name, kind) = (name.unwrap(), kind.unwrap());

        match Channel::add(pool, name, description, kind).await {
            Ok(resp) => {
                match resp {
                    Row(channel) => {
                        #[cfg(feature = "rtc")]
                        {
                            use crate::rtc;
                            rtc::create_channel(channel.clone()).await;
                        }
                        set_json_body(response, json!({"channel": channel}));
                        StatusCode::OK
                    },
                    RestrictedInput(_) => StatusCode::UNPROCESSABLE_ENTITY,
                    // Unreachable
                    _ => {
                        eprintln!("[HTTP] /channels/create Couldn't fetch response variant");
                        StatusCode::INTERNAL_SERVER_ERROR
                    }
                }
            },
            Err(e) => {
                eprintln!("Error: {}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    } else {
        StatusCode::BAD_REQUEST
    }
}

pub async fn delete_channel(pool: &Pool, response: &mut Response<Body>, params: HashMap<String, String>) {
    /*
     * Deletes a channel from the database, only after making sure the user has
     * the required permissions to do so
     * @channel_id : u64 - required
    */
    use crate::perms;
    use db::Member;
    use db::Response::*;

    let uid = qs_param!(params, "id", u64).unwrap();
    let permissions = match Member::get(pool, uid).await {
        Ok(Row(user)) => user.permissions,
        Ok(_) => 0,
        Err(e) => {
            eprintln!("{}", e);
            0
        }
    };

    // make sure unpriveleged users don't delete channels somehow
    if perms::has_perm(permissions, perms::DELETE_CHANNEL) == false{
        *response.status_mut() = StatusCode::BAD_REQUEST;
        return;
    }

    // Collect the channel_id param before we attempt deletion
    let channel_id = if let Some(chan) = params.get("channel_id") {
        let c = chan;
        match c.to_string().parse::<u64>() {
            Ok(val) => Some(val),
            _ => None
        }
    } else {
        None
    };

    if let Some(id) = channel_id {
        match Channel::delete(pool, id).await {
            #[cfg(feature = "rtc")]
            Ok(Success) => {
                use crate::rtc;
                rtc::delete_channel(id).await;
            },
            Ok(_) => {/* can't actually happen */}
            Err(e) => { // ngmi
                eprintln!("{}", e);
                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
            }
        }     
    }
    else {
        *response.status_mut() = StatusCode::BAD_REQUEST;
    }
}

