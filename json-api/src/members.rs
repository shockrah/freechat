use std::collections::HashMap;
use hyper::{Response, Body, StatusCode};
use mysql_async::Pool;
use serde_json::json;

use db::member::STATUS_ONLINE;
use db::{PublicMember, Member};
use db::Response::*;

use crate::http::set_json_body;
use crate::qs_param;


pub async fn get_online_members(p: &Pool, response: &mut Response<Body>) {
    // Lists out the available online members at the time
    // Member structs are wildly small so for most cases this query really isn't that expensive
    match PublicMember::get_online(p, STATUS_ONLINE).await {
        Ok(dbresponse) => {
            match dbresponse {
                Set(users) => set_json_body(response, json!(users)),
                Other(e) => {
                    eprintln!("{}", e);
                    *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                },
                // Unreachable code but required for rustc to be happy
                _ => *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR
            };
        },
        Err(e) => eprintln!("Sql error: {}", e)
    };
}

pub async fn get_self(p: &Pool, response: &mut Response<Body>, params: HashMap<String, String>) {
    let uid = qs_param!(params, "id", u64).unwrap();

    match Member::get(p, uid).await {
        Ok(dbresponse) => {
            match dbresponse { 
                Row(user) => {
                    set_json_body(response, json!({
                        "id" : user.id,
                        "name" : user.name,
                        "permissions" : user.permissions
                    }));
                },
                Other(e) => eprintln!("{}", e),
                _ => {}
            }
        },
        Err(e) => {
            eprintln!("[HTTP-DB-ERROR] {}", e);
            *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
        }
    };
}

pub async fn post_self_nickname(p: &Pool, response: &mut Response<Body>, params: HashMap<String, String>) {
    /*
     * @param nick <String>
     * @respond StatusCode
     *
     * Prety straightforward response here as its literally just the status code
     * Assuming we have good input
     */

    let nick = qs_param!(params, "nick", String);

    *response.status_mut() = if let Some(nick) = nick {

        let uid = qs_param!(params, "id", u64).unwrap();
        match Member::update_nick(p, uid, nick.as_ref()).await {
            #[cfg(feature = "rtc")]
            Ok(_) => {
                use crate::rtc;
                rtc::update_nickname(uid, nick.as_str()).await;
                StatusCode::OK
            },
            #[cfg(not(feature = "rtc"))]
            Ok(_) => StatusCode::OK,
            Err(e) => {
                eprintln!("{}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    } else {
        StatusCode::BAD_REQUEST
    }
}

pub async fn get_member(p: &Pool, response: &mut Response<Body>, params: HashMap<String, String>) {
    let target = qs_param!(params, "member_id", u64);

    // Returning from from if let for the user parameter parsing
    *response.status_mut() = if let Some(target) = target{
        match Member::get(p, target).await {
            Ok(dbresponse) => match dbresponse {
                Row(member) => {
                    set_json_body(response, json!({"member": {
                        "id": member.id,
                        "name": member.name,
                        "status": member.status, 
                        "permissions": member.permissions
                    }}));
                    StatusCode::OK
                },
                Empty => StatusCode::NOT_FOUND,
                Other(_msg) => StatusCode::INTERNAL_SERVER_ERROR,
                _ => StatusCode::INTERNAL_SERVER_ERROR
            },
            Err(e) => {
                eprintln!("[HTTP-DB-ERROR] {}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    } else {
        StatusCode::BAD_REQUEST
    }
}

#[cfg(test)]
mod tests {
    use crate::testing::get_pool;
    use crate::testing::hyper_resp;
    use std::collections::HashMap;
    use tokio;

    #[tokio::test]
    async fn get_member_test() {
        // mostly looking for correct response code here
        // NOTE: we are assuming that the database we're trying against has 
        // some dummy data in it for us to actually look at

        let p = get_pool();
        let mut resp = hyper_resp();
        let mut params: HashMap<String, String> = HashMap::new();
        params.insert("member_id".into(), "1".into());

        super::get_member(&p, &mut resp, params).await;

        println!("Response: {:?}", resp);
    }
}

