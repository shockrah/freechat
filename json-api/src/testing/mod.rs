// Functions which are only really useful for the unit tests but which show up 
// constantly in the tests themselves

#[cfg(test)]
pub fn get_pool() -> mysql_async::Pool {
    // NOTE: this assumes that DATABASE_URL has been set in the environment
    // prior to running
    use mysql_async::Pool;

    return Pool::new(&std::env::var("DATABASE_URL").unwrap())
}

#[cfg(test)]
pub fn hyper_resp() -> hyper::Response<hyper::Body> {
    use hyper::{Response, Body};

    Response::new(Body::empty())
}


