use crate::api_types as api;
use std::fs::OpenOptions;
use std::io::prelude::*;

const LOG_FILE: &'static str = "tui.log";

pub fn file(data: &api::Message) {
    let mut file = OpenOptions::new()
        .append(true)
        .create(true)
        .open(LOG_FILE).unwrap();

    let _ = file.write(format!("{:?}", data).as_bytes());
}

pub fn plain(s: String) { 
    let mut file = OpenOptions::new()
        .append(true)
        .create(true)
        .open(LOG_FILE).unwrap();

    let _ = file.write(format!("{}\n", s).as_bytes());
}
