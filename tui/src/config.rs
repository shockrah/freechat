use serde::{Serialize, Deserialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ServerMeta {
    pub url: String, // Should never end with a '/'
    pub description: String,
    pub name: String, 
}


#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UserMeta {
    pub id: u64,
    pub secret: String,
    pub jwt: Option<String>,

    pub permissions: u64,
    pub status: i32,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ServerConfig {
    pub user: UserMeta,
    pub server: ServerMeta
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ConfigFile {
    // Global option
    pub username: Option<String>,
    pub servers: Vec<ServerConfig>
}


impl ConfigFile {
    pub fn save(&self, path: String) -> std::io::Result<()>{
        use std::io::prelude::Write;
        use std::fs::File;

        let content = serde_json::to_string_pretty(self)?;
        let mut file = File::create(path)?;
        file.write(content.as_bytes())?;

        Ok(())
    }

    pub fn find_login_data(&self, url_portion: &str) -> Option<(u64, String, String)> {
        let mut found: Option<(u64, String, String)> = None;
        for serv in self.servers.iter() {
            if serv.server.url.contains(url_portion) {
                found = Some((serv.user.id, serv.user.secret.clone(), serv.server.url.clone()))
            }
        }
        return found;
    }
}


