use serde::Deserialize;

#[allow(dead_code)]
const VOICE_CHANNEL: i32 = 1;
pub const TEXT_CHANNEL: i32 = 2;

// Network Types
#[derive(Debug)]
pub struct Message {
    pub id: u64,
    pub time: i64,
    pub content: String,
    pub content_type: String,
    pub channel_id: u64,
    pub userid: u64,
    pub username: String,
}

impl Message {
    pub fn bogus(s: String) -> Message {
        Message {
            id: 1,
            time: 1,
            content: s,
            content_type: "text/plain".into(),
            channel_id: 1,
            userid: 1,
            username: "yes".into()
        }
    }
}


#[derive(Debug, Deserialize, Clone)]
pub struct Channel {
    pub name: String,
    pub id: u64,
    pub kind: i32,
    pub description: Option<String>,
}

#[derive(Deserialize)]
pub struct Jwt {
    pub jwt: String
}
