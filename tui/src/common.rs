#[macro_export]
macro_rules! bold {
    ($text:expr) => {
        Span::styled($text, Style::default().add_modifier(Modifier::BOLD))
    }
}

#[macro_export]
macro_rules! normal {
    ($text:expr) => {
        Span::raw($text)
    }
}
