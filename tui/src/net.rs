use serde::Deserialize;
use crate::api_types::{Jwt, Channel, TEXT_CHANNEL};
use reqwest::{Client, Url, Body, Response};
use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE};
use reqwest::Result as HttpResult;


pub async fn login(url: &str, id: u64, secret: &str) -> HttpResult<String> {
    let client = Client::new();
    let url = Url::parse_with_params(
        &format!("{}/login", url), 
        &[("id", format!("{}", id).as_str()), ("secret", secret)]
    ).unwrap();


    let response: Jwt  = client.get(url).send().await?.json().await?;
    Ok(response.jwt)
}

pub async fn list_channels(url: &str, id: u64, jwt: &str)  -> HttpResult<Vec<Channel>> {
    let client = Client::new();
    let url = Url::parse_with_params(
        &format!("{}/channels/list", url),
        &[
            ("id", format!("{}", id).as_str()), 
            ("jwt", jwt), 
            ("kind", format!("{}", TEXT_CHANNEL).as_str())
        ]
    ).unwrap();

    #[derive(Deserialize)]
    struct ChannelResponse {
        pub channels: Vec<Channel>
    }
    let response: ChannelResponse  = client.get(url).send().await?.json().await?;
    Ok(response.channels)
}

pub async fn send_text<'m>(url: &str, id: u64, jwt: &str, chan: u64, msg: &str) -> HttpResult<Response> {
    let client = Client::new();
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_str("text/plain").unwrap());
    let url = Url::parse_with_params(
        &format!("{}/message/send", url),
        &[
            ("id", format!("{}", id).as_str()),
            ("jwt", jwt),
            ("channel_id", &format!("{}", chan))
        ]
    ).unwrap();

    let body = Body::from(msg.to_string());
    Ok(client.post(url).body(body).headers(headers).send().await?)
}

#[allow(dead_code)]
pub fn ws_url(base: &str, jwt: Option<String>) -> String {
    let base = format!("{}/text",  base);
    let jwt = jwt.unwrap_or("hehexd".into());
    let mut url = Url::parse_with_params(base.as_ref(), &[("jwt", jwt.as_str())]).unwrap();
    let _ = url.set_port(Some(5648));
    let _ = url.set_scheme("ws");
    url.to_string()
}

