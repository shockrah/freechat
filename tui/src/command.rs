use crate::{bold, normal};
use tui::text::{Span, Spans};
use tui::style::{Style, Modifier};

pub enum Command {
    Help,
    // Picking based on id
    Channel(u64),
    ListChannel, // app only lists if the cache has an active server
    // Choose server based on hostname
    Server(String),
    // List out available hosts
    ListHost,
    Text(String),
    // Send regular message
    Message(String),
    // Command that failed with some message
    Failure(String),
}

impl Command {
    // Pulls out channel id from a line
    // Examples: /chan 123
    // /channel 789
    // /channelswag 1
    fn parse_chan_id(s: &str) -> Option<u64> {
        let parts: Vec<&str> = s.split(" ").collect();
        return if parts.len() < 2 {
            None
        } else {
            let id_s = parts.get(1).unwrap();
            match id_s.parse::<u64>() {
                Ok(id) => Some(id),
                _ => None
            }
        }
    }

    fn parse_hostname(s: &str) -> Option<String> {
        let parts: Vec<&str> = s.split(" ").collect();
        return if parts.len() < 2{
            None
        } else{
            let hostname: String = (*parts.get(1).unwrap()).into();
            Some(hostname)
        }
    }

    pub fn from(s: &str) -> Command {
        let s = s.trim();
        // NOTE: could probably do this smarter but a handful of string comparisons is fine too
        if s.starts_with("/chan") {
            match Command::parse_chan_id(s.as_ref()) {
                Some(id) => Command::Channel(id),
                None => Command::Failure("no valid id(u64) provided".into())
            }
        } else if s.starts_with("/serv") {
            match Command::parse_hostname(s.as_ref()) {
                Some(hostname) => Command::Server(hostname),
                None => Command::Failure("no hostname provided".into())
            }
        } else if s.starts_with("/help") {
            Command::Help
        } else if s.starts_with("/lh") || s.starts_with("/listhost") {
            Command::ListHost
        } else if s.starts_with("/lc") || s.starts_with("/listchan") {
            Command::ListChannel
        }
        else {
            if s.starts_with("/") {
                Command::Failure("command not found".into())
            } else {
                Command::Message(s.into())
            }
        }
    }

    pub fn styled(&self) -> Spans {
        use Command::*;
        return match self {
            Help => Spans::from(bold!("! /help ! /channel <u64> ! /server <u64>")),
            Text(s) => Spans::from(vec![normal!(s)]),
            Channel(id) => Spans::from(vec![
                bold!("! /channel "),
                normal!(format!("{}", id)),
            ]),
            Server(hostname) => Spans::from(vec![
                bold!("! /server "),
                normal!(hostname)
            ]),
            ListHost => Spans::from(vec![
                bold!("! /lh"),
            ]),
            Message(msg) => Spans::from(vec![
                bold!("(You) "), normal!(msg)
            ]),
            ListChannel => Spans::from(vec![normal!("/lc - list out (text)channels in server")]),
            Failure(msg) => Spans::from(vec![
                bold!("! error "),
                normal!(msg)
            ])
        }
    }
}
