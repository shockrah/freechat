const ws = require('ws')
const _ = require('./cmdline.js') // initializing the command line params first
const auth = require('./auth.js')

const server = new ws.Server({ 
	port: 5648,
	clientTracking: true
})

server.on('connection', function(socket, req) {
	const [jwt, path] = auth.prepare_auth(req)
	const conn = auth.verify(jwt)
	if(conn == 'server') {
		console.log('[WSS] New server connection')

		socket.on('close', function() {
			console.log('[WSS] Server connection closed')
		})

		socket.on('message', function(message) {
			// do some parsing, then emit to everyone
			server.clients.forEach(client => {
				if (client !== socket && client.readyState === ws.OPEN) {
					client.send(message)
				}
			})
		})

	} else if(conn == 'user') {
		console.log('[WSS] New user connection')
		socket.on('close', function() {
			console.log('[WSS] User connection closed')
		})
	} else {
		console.log('[WSS] No valid auth', conn)
		socket.close()
	}
	
})
