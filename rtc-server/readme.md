# RTC WebSocket Server

This server is to be used _only_ if the `json-api` server is compiled with the `rtc` feature.
If you run this server without that feature in the `json-api` then this will never get updates from the API and thus be completely 100% useless.

# Configuration

This server runs on port 5648 and is served via the `ws://` protocol.
It is recommended that admins put this behind some a reverse proxy for secure web-sockets `wss://` and to remove the need for specifying the port number for clients.

## Command line flags

-p --port <PORT> : Defaults to 5648 (suggested)

