const argparse = require('argparse')

// Command line parsing first
const parser = new argparse.ArgumentParser({
	description: 'RTC Server Component for freechat'
})
parser.add_argument('-w', '--wss-hmac', { help: 'Set the path for the websocket hmac'})
parser.add_argument('-H', '--hmac', { help: 'Set the path for the API-server hmac'})
const args = parser.parse_args()

// hmac paths are set/overridden through the commandline parameters
console.log(args)
if(args['wss_hmac'] != undefined) {
	process.env['WSS_HMAC'] = args['wss_hmac']
}
if(args['hmac'] != undefined) {
	process.env['HMAC'] = args['hmac']
}

